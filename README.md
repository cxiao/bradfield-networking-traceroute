# trout

A traceroute clone. Currently only does ICMP ping probes.

```
$ sudo ./trout bad.horse
trout swimming to bad.horse (162.252.205.157)
[...]
  9  any2-la.cr1.lax0.amcbb.net (206.72.210.125)  38.552 ms  39.573 ms  38.507 ms
  10  ve17.cr1.lax0.atlanticmetro.net (208.78.31.233)  42.677 ms  40.838 ms  38.504 ms
  11  ve991.cr2.lga3.atlanticmetro.net (208.78.31.225)  94.118 ms  89.984 ms  97.775 ms
  12  e6-3.cr1.lga12.atlanticmetro.net (108.60.148.78)  105.072 ms  99.271 ms  88.109 ms
  13  e2-20.cr2.lga11.atlanticmetro.net (69.9.32.221)  90.269 ms  89.236 ms  93.819 ms
  14  t03.nycmc1.ny.us.sn11.net (162.252.205.4)  129.147 ms  127.180 ms  127.397 ms
  15  bad.horse (162.252.205.130)  136.194 ms  138.459 ms  138.772 ms
  16  bad.horse (162.252.205.131)  149.568 ms  144.380 ms  133.600 ms
  17  bad.horse (162.252.205.132)  149.259 ms  152.596 ms  144.007 ms
  18  bad.horse (162.252.205.133)  165.998 ms  158.917 ms  146.374 ms
  19  he.rides.across.the.nation (162.252.205.134)  155.038 ms  155.470 ms  156.037 ms
  20  the.thoroughbred.of.sin (162.252.205.135)  159.835 ms  160.157 ms  156.880 ms
  21  he.got.the.application (162.252.205.136)  157.948 ms  162.219 ms  158.654 ms
  22  that.you.just.sent.in (162.252.205.137)  172.459 ms  173.532 ms  172.487 ms
  23  it.needs.evaluation (162.252.205.138)  178.616 ms  169.882 ms  168.907 ms
  24  so.let.the.games.begin (162.252.205.139)  183.922 ms  175.419 ms  174.302 ms
  25  a.heinous.crime (162.252.205.140)  185.918 ms  177.989 ms  199.045 ms
  26  a.show.of.force (162.252.205.141)  182.900 ms  191.819 ms  192.001 ms
  27  a.murder.would.be.nice.of.course (162.252.205.142)  195.259 ms  186.012 ms  198.738 ms
  28  bad.horse (162.252.205.143)  206.651 ms  205.380 ms  198.752 ms
  29  bad.horse (162.252.205.144)  199.727 ms  210.201 ms  207.551 ms
  30  bad.horse (162.252.205.145)  203.907 ms  203.141 ms  203.764 ms
  31  he-s.bad (162.252.205.146)  222.138 ms  211.882 ms  209.795 ms
  32  the.evil.league.of.evil (162.252.205.147)  218.296 ms  225.141 ms  225.086 ms
  33  is.watching.so.beware (162.252.205.148)  248.268 ms  215.513 ms  220.627 ms
  34  the.grade.that.you.receive (162.252.205.149)  231.985 ms  219.917 ms  224.154 ms
  35  will.be.your.last.we.swear (162.252.205.150)  235.152 ms  232.354 ms  245.311 ms
  36  so.make.the.bad.horse.gleeful (162.252.205.151)  232.292 ms  232.127 ms  244.321 ms
  37  or.he-ll.make.you.his.mare (162.252.205.152)  258.598 ms  252.545 ms  243.840 ms
  38  o_o (162.252.205.153)  262.007 ms  248.852 ms  258.230 ms
  39  you-re.saddled.up (162.252.205.154)  265.171 ms  250.424 ms  275.162 ms
  40  there-s.no.recourse (162.252.205.155)  254.540 ms  256.670 ms  253.719 ms
  41  it-s.hi-ho.silver (162.252.205.156)  260.189 ms  261.330 ms  268.141 ms
  42  signed.bad.horse (162.252.205.157)  260.114 ms  259.065 ms  256.296 ms
```